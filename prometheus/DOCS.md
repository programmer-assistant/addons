# Programmer Assistant HASS Add-on: Prometheus

Prometheus - cloud native metrics server

## Installation

The installation of this add-on is pretty straightforward and not different in
comparison to installing any other Home Assistant add-on.

1. Search for the "Prometheus" add-on in the Supervisor add-on store and install it.
1. Start the "Prometheus" add-on.
1. Check the logs of the "Prometheus" add-on to see if everything went well.
1. Click the "OPEN WEB UI" button!

## Configuration

**Note**: _Remember to restart the add-on when the configuration is changed._

Addon is pre-configured to scrap data from home assistant and you don't need any
additional configuration to start use it.

To add additional endpoints create `.yml` file in `/share/prometheus/targets/` and put list of jobs
like in below example.

```yaml
---
- labels:
    job: raspberrypi-1
  targets:
  - 192.168.1.10:9100
- labels:
    job: raspberrypi-1
  targets:
  - 192.168.1.12:9100
```

Additional can be made in location `/share/prometheus/rules/` and `/share/prometheus/targets/`.
Addon reads a set of files from `targets` frolder containing a list of zero or more YAML or JSON format `<static_config>`s. [read more][file_sd_config]

## Changelog & Releases

This repository keeps a change log using [CHANGELOG.md](CHANGELOG.md)
functionality.

Releases are based on [Semantic Versioning][semver], and use the format
of `MAJOR.MINOR.PATCH`. In a nutshell, the version will be incremented
based on the following:

- `MAJOR`: Incompatible or major changes.
- `MINOR`: Backwards-compatible new features and enhancements.
- `PATCH`: Backwards-compatible bugfixes and package updates.

## Support

Got questions?

[Open an issue here][issue] on GitLab.

## Authors & contributors

The original setup of this repository is by [Dawid Rycerz][knightdave].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## License

MIT License

Copyright (c) 2021-2021 Dawid Rycerz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[semver]: http://semver.org/spec/v2.0.0.html
[issue]: https://gitlab.com/programmer-assistant/addons/-/issues
[comminuty-addons]: https://addons.community
[contributors]: https://gitlab.com/programmer-assistant/addons/-/graphs/master
[knightdave]: https://gitlab.com/knightdave
[file_sd_config]: https://prometheus.io/docs/prometheus/latest/configuration/configuration/#file_sd_config