# Programmer Assistant HASS Add-on: Pi-hole

The Pi-hole® is a DNS sinkhole that protects your devices from unwanted content
without installing any client-side software.

## Installation

The installation of this add-on is pretty straightforward and not different in
comparison to installing any other Home Assistant add-on.

1. Search for the "Pi-hole" add-on in the Supervisor add-on store and install it.
1. Start the "Pi-hole" add-on.
1. Check the logs of the "Pi-hole" to get web password.
1. Click the "OPEN WEB UI" button!
1. Login with login with password generated in logs.

## Configuration

**Note**: _Remember to restart the add-on when the configuration is changed._

Example add-on configuration:

```yaml
env_vars:
  - name: WEBPASSWORD
    value: "!secret pihole_webpassword"
  - name: ServerIP
    value: "192.168.1.10"
ssl: true
certfile: fullchain.pem
keyfile: privkey.pem
```

### Option: `env_vars`

This option allows you to tweak every aspect of Pi-hole Server by setting
configuration options using environment variables. See the example at the
start of this chapter to get an idea of how the configuration looks.

For more information about using these variables, see the official Pi-hole Server
documentation:

<https://github.com/pi-hole/docker-pi-hole#environment-variables>


## Changelog & Releases

This repository keeps a change log using [CHANGELOG.md](CHANGELOG.md)
functionality.

Releases are based on [Semantic Versioning][semver], and use the format
of `MAJOR.MINOR.PATCH`. In a nutshell, the version will be incremented
based on the following:

- `MAJOR`: Incompatible or major changes.
- `MINOR`: Backwards-compatible new features and enhancements.
- `PATCH`: Backwards-compatible bugfixes and package updates.

## Support

Got questions?

[Open an issue here][issue] on GitLab.

## Authors & contributors

The original setup of this repository is by [Dawid Rycerz][knightdave].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## License

MIT License

Copyright (c) 2021-2022 Dawid Rycerz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[semver]: http://semver.org/spec/v2.0.0.html
[issue]: https://gitlab.com/programmer-assistant/addons/-/issues
[contributors]: https://gitlab.com/programmer-assistant/addons/-/graphs/master
[knightdave]: https://gitlab.com/knightdave
