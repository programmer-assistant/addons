# Programmer Assistant HASS Add-ons

Add-ons for Home Assistant, allow you to extend the functionality
around your Home Assistant setup. These add-ons are set of handy tools for programmers.

Add-ons can be installed and configured via the Home Assistant frontend on
systems that have installed Home Assistant.

## Add-ons provided by this repository

- **[Prometheus](/promethes/README.md)**

   Systems monitoring and alerting toolkit.

- **[MinIO](/minio/README.md)**

   High Performance Object Storage.

- **[Registry](/registry/README.md)**

   Docker registry configured to run as cache.

- **[Bazel remote](/bazel-remote/README.md)**

   Bazel remote cache http server.
